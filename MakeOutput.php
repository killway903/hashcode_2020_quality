<?php

class MakeOutput
{
    public function create($answers)
    {

        $output = '';

        foreach ($answers as $key => $item) {
            if ($key === 'libraries') {
                $output .= $item . "\n";
            } else {
                $output .= $item['lib_id'] . ' ' . $item['number_of_books'] . "\n";

                if (isset($item['books'])) {
                    foreach ($item['books'] as $k => $book) {
                        $output .= $book;
                        if (isset($item['books'][$k + 1])) {
                            $output .= ' ';
                        }
                    }
                    $output .= "\n";
                }
            }
        }

        file_put_contents(__DIR__ . '/results/' . fileName . '.txt', $output);
    }
}