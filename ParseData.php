<?php

class ParseData
{
    public function parse()
    {
        define('fileName', 'b_read_on');

        $fileString = fopen('./inputs/' . fileName . '.txt', 'r');
        $fileStrings = [];

        while (!feof($fileString)) {
            $line = fgets($fileString);
            $fileStrings[] = $line;
        }

        fclose($fileString);

        $returnData = [];
        $libId = 0;
        foreach ($fileStrings as $k => $string) {
            $splitString = explode(' ', $string);

            if ($string) {
                if ($k === 0) {
                    $returnData['books_total'] = $splitString[0];
                    $returnData['number_of_libraries'] = $splitString[1];
                    $returnData['number_of_days'] = $splitString[2];
                } else if ($k === 1) {
                    foreach ($splitString as $bookId => $bookScore) {
                        $returnData['books_score'][] = $bookScore;
                    }
                } elseif ($k % 2 == 0) {
                    if (isset($splitString[1]))
                        $returnData['libs'][$libId] = [
                            'lib_id' => $libId,
                            'number_of_books' => $splitString[0],
                            'sign_up_days' => $splitString[1],
                            'books_per_day' => $splitString[2]
                        ];
                } elseif ($k > 1 && $k % 2 !== 0) {
                    foreach ($splitString as $book_id) {
                        $returnData['libs'][$libId]['books'][] = ['id' => (int)$book_id, 'score' => $returnData['books_score'][(int)$book_id]];
                    }
                    $libId++;
                }
            }
        }
        $returnData['book_average_score']= array_sum($returnData['books_score']) / count($returnData['books_score']);
//        $returnData['libs'] = array_values($returnData['libs']);
        return $returnData;
    }
}