<?php

require_once 'ParseData.php';
require_once 'CountLibraryScore.php';
require_once 'Core.php';
require_once 'MakeOutput.php';

$parsedData = (new ParseData())->parse();

$parsedData['libs'] = (new CountLibraryScore())->count($parsedData);

$answers = (new Core())->foundAnswer($parsedData);

(new MakeOutput())->create($answers);

