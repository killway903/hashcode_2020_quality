<?php

class Core
{
    public function foundAnswer($parsedData)
    {

        $days = 0;

        usort($parsedData['libs'], function ($a, $b) {
            return $b['sign_up_days'] - $a['sign_up_days'];
        });

        $answers = [];
        $answers['libraries'] = 0;
        $usedBooks = [];

        while ($days < $parsedData['number_of_days']) {

            foreach ($parsedData['libs'] as $lib) {
                $lib_id = $lib['lib_id'];

                usort($lib['books'], function ($a, $b) {
                    return $a['score'] <= $b['score'];
                });

                $answers[$lib_id]['lib_id'] = $lib_id;
                $answers[$lib_id]['number_of_books'] = 0;
                $answers[$lib_id]['sign_up_day'] = $days;

                $counter = 0;

                foreach ($lib['books'] as $book) {

//                if ($parsedData['book_average_score'] <= $parsedData['books_score'][$book['id']])
                    if (!in_array($book['id'], $usedBooks)) {
                        $answers[$lib_id]['books'][] = $book['id'];
                        $answers[$lib_id]['number_of_books']++;
                        $usedBooks[] = $book['id'];

                        if ($counter >= $lib['books_per_day']) {
                            $days++;
                        }

                        $counter++;
                    }
                }

                $answers['libraries']++;

                $days += $lib['sign_up_days'];

                if ($days > $parsedData['number_of_days']) {
                    break;
                }
            }
        }

        return $answers;
    }
}